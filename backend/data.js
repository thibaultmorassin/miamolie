const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure
const Recipe = new Schema(
    {
        title: String,
        people: Number,
        category: String,
        subCategory: String,
        time: String,
        price: Number,
        ingredient: String,
        recipe: String,
        description: String,
        photo: String,
        count: Number,
    },
    {timestamps: true}
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("Data", Recipe);